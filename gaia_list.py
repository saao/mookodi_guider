#python3 -m pip install astroquery
# sep and photoutils were already istalled

# to run on mookodi use this virtual env:
#source /home/mookodi/development/WCS/pyenv/wcsenv/bin/activate

from guider import Guider
import glob
import os
import argparse
import sys
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
import datetime
import numpy as np
import matplotlib.pyplot as plt
import astropy.visualization
zscaler = astropy.visualization.ZScaleInterval()
from photutils import CircularAperture
import sep
from collections import OrderedDict
import math 
from time import sleep
import socket
import signal # used to capture ctrl-c and exit nicely by closing connection


from astroquery.gaia import Gaia


def get_XYslide_of_brightest_gaia(wcs_ra, wcs_dec, rotator_angle):  #get_XYslide_of_brightest_gaia(wcs_ra, wcs_dec, rotator_angle)
    
    telescope_coords = SkyCoord(wcs_ra, wcs_dec,unit=(u.hourangle, u.deg))
    ra_in_deg = telescope_coords.to_string().split()[0]
    dec_in_deg = telescope_coords.to_string().split()[1]

    print('wcs Telescope RA: '+ ra_in_deg)
    print('wcs Telescope DEC: '+ dec_in_deg)
    
    tel_ra_in_deg = float(ra_in_deg)
    tel_dec_in_deg = float(dec_in_deg)


    ##############################################
    # Get list of gaia source around tel ra and dec ... 40arcmins width 40arcmins height (actually 1x1 degree)
    # xyslides only have access to upper or lower half depending on rotator angle
    Gaia.MAIN_GAIA_TABLE = "gaiaedr3.gaia_source" # Select early Data Release 3
    coord = SkyCoord(ra=tel_ra_in_deg, dec=tel_dec_in_deg, unit=(u.degree, u.degree), frame='icrs')
    width = u.Quantity(1.0, u.deg) # 40arcmins = 0.667 degs
    height = u.Quantity(1.0, u.deg) # make it 1 degree as it looks like the slides are not centered 
    Gaia.ROW_LIMIT = 80000
    r = Gaia.query_object_async(coordinate=coord, width=width, height=height)

    gaia_star_info = np.rec.fromarrays([ r['source_id'].data,
                                r['ra'].data,
                                r['dec'].data,
                                r['phot_rp_mean_mag'].data],
                                dtype=None,names=['source_id','ra','dec','phot_rp_mean_mag'])

    print(" no_gaia_sources  : ", len(gaia_star_info) )        


    if ( (rotator_angle > -2.0) and (rotator_angle < 2.0) ): # rotator angle around 0
        #  only select stars with dec < tel_dec and brighter than 16 ... for rotang near zero, target is at the top
        half_gaia_star_info = (gaia_star_info[np.where((gaia_star_info['dec']<tel_dec_in_deg) & (gaia_star_info['phot_rp_mean_mag']<16)  )])
    elif ( (rotator_angle > 178) or (rotator_angle < -178) ): # rotator angle around 180
        #  only select stars with dec > tel_dec and brighter than 16 ... for rotang near 180, target is at the bottom
        half_gaia_star_info = (gaia_star_info[np.where((gaia_star_info['dec']>tel_dec_in_deg) & (gaia_star_info['phot_rp_mean_mag']<16)  )])

    print(" no_gaia_sources brighter than 16 and south of tel_dec  : ", len(half_gaia_star_info) )    

    #print(half_gaia_star_info)
    
    # convert gaia  RAs to dec corrected RAs. RA aleady in degrees so no need for factor of 15
    half_gaia_star_info['ra'] = half_gaia_star_info['ra']  * math.cos(tel_dec_in_deg*math.pi/180.)
    # convert tel  RA to dec corrected RA. RA aleady in degrees so no need for factor of 15
    tel_ra_in_deg_sky = tel_ra_in_deg * math.cos(tel_dec_in_deg*math.pi/180.)
    
    # subtract tel ra and dec from gaia coords 
    ra_dist =   half_gaia_star_info['ra'] - tel_ra_in_deg_sky
    dec_dist =  half_gaia_star_info['dec'] - tel_dec_in_deg
    
    # - transform RA,Dec to xyslide x,y:  for rotator angle around 0
    ### RApopt[0],RApopt[1] =  -2.8275789693768107e-05 0.2692593231011655    from on-sky data and fits
    #### DECpopt[0],DECpopt[1] =  2.8279005498598875e-05 -0.4196063511127433    from on-sky data and fits
    #### RApopt[0],RApopt[1] =  2.8338318048924984e-05 -0.26546256702498733  rotang 180
    #### DECpopt[0],DECpopt[1] =  -2.8441796558404097e-05 0.4257036922377935 rotang 180

    #### see /Users/sbp/Lesedi/xyslides2ports/gaia_list/data.py

    if ( (rotator_angle > -2.0) and (rotator_angle < 2.0) ): # rotator angle around 0
        xa = -2.8275789693768107e-05
        xc = 0.2692593231011655
        x_slide = (ra_dist-xc)/xa
        ya = 2.8279005498598875e-05
        yc = -0.4196063511127433
        y_slide = (dec_dist-yc)/ya
    elif ( (rotator_angle > 178) or (rotator_angle < -178) ): # rotator angle around 180
        xa = 2.8338318048924984e-05
        xc = -0.26546256702498733
        x_slide = (ra_dist-xc)/xa
        ya = -2.8441796558404097e-05
        yc = 0.4257036922377935
        y_slide = (dec_dist-yc)/ya

        
    ## overwrite the ra dec with x and y slide in the record array ... makes it easier to filter below
    half_gaia_star_info['ra'] = x_slide
    half_gaia_star_info['dec'] = y_slide

    #### filter to select stars only within xyslides 3100,13000 .... bizzare ... why also ra>0 ????
    #half_gaia_star_info=half_gaia_star_info[np.where((half_gaia_star_info['ra']>0) & (half_gaia_star_info['ra']<13000) & (half_gaia_star_info['ra']>3100))]
    #half_gaia_star_info=half_gaia_star_info[np.where((half_gaia_star_info['dec']>0) & (half_gaia_star_info['dec']<13000) & (half_gaia_star_info['dec']>3100))]


    #### filter to select stars only within xyslides   2000<x<14000 & 2000<y<6000   PLUS(note not AND)   2000<x<6000 & 2000<y<13000
    half_gaia_star_info_low=half_gaia_star_info[np.where( (half_gaia_star_info['ra']<14000)  & (half_gaia_star_info['ra']>2000))]
    #half_gaia_star_info_low=half_gaia_star_info_low[np.where( (half_gaia_star_info_low['dec']<8000) & (half_gaia_star_info_low['dec']>4000))]
    half_gaia_star_info_low=half_gaia_star_info_low[np.where( (half_gaia_star_info_low['dec']<12000) & (half_gaia_star_info_low['dec']>4000))]

    #half_gaia_star_info_left=half_gaia_star_info[np.where( (half_gaia_star_info['ra']<5000)  & (half_gaia_star_info['ra']>2000))]
    half_gaia_star_info_left=half_gaia_star_info[np.where( (half_gaia_star_info['ra']<8000)  & (half_gaia_star_info['ra']>2000))]
    half_gaia_star_info_left=half_gaia_star_info_left[np.where( (half_gaia_star_info_left['dec']<13000) & (half_gaia_star_info_left['dec']>4000))]

    #half_gaia_star_info_final = half_gaia_star_info_low + half_gaia_star_info_left
    #print(half_gaia_star_info)

    # - select brightest star
    #max_bright_index = np.argmin(half_gaia_star_info['phot_rp_mean_mag'])


    max_bright_index_left = np.argmin(half_gaia_star_info_left['phot_rp_mean_mag'])  
    brightest_left = half_gaia_star_info_left['phot_rp_mean_mag'][max_bright_index_left] # britest star in left area
    max_bright_index_low = np.argmin(half_gaia_star_info_low['phot_rp_mean_mag'])  
    brightest_low = half_gaia_star_info_low['phot_rp_mean_mag'][max_bright_index_low] # britest star in low area

    if (brightest_low < brightest_left ):
        return(half_gaia_star_info_low[max_bright_index_low])
    else:
        return(half_gaia_star_info_left[max_bright_index_left])


def pos_XYslides(xslide, yslide, Mag): 

    #wcs_ra = "09:00:00"   # testing
    #wcs_dec = "-50:00:14"  # testing
    #rotation_angle = 0.1  # testing

    #wcs_ra = "09:00:01"  # testing
    #wcs_dec = "5:00:11"  # testing
    #rotation_angle = 179.  # testing

    if "None" in xslide: # either xslide or yslide =None (note None is a string here) means there was no precalculated coordinates
        # therefore have to do the whole gaia search and calulation here
    
        ############  Obtain the RA, Dec, rotator_anlge of telescope from the WCS solution ######

        ################## create list of the .WCS files ####################
        file_list=glob.glob("/home/mookodi/development/WCS/fits_files/*.WCS")
        sorted_file_list = sorted(file_list) # sort the list into alphabetical/numeric order
        sorted_file_list.reverse()           # reverse the list so that latest file is first
        ####################################################################################

        ### open the last WCS file and read in the WCS parameters ####
        with open(sorted_file_list[0],'r') as f:
            WCS_lines = f.readlines()
        f.close()

        ##### extract the rotation angle ############
        numbers = []
        for lines in WCS_lines:
            if "rotation angle" in lines:
                for sub_string in lines.split():
                    try:
                        numbers.append(float(sub_string))
                    except ValueError:
                        pass
        rotation_angle = numbers[0]

        ##### extract RA H:M:S, Dec D:M:S   ############
        numbers = []
        for lines in WCS_lines:
            if "(RA H:M:S, Dec D:M:S)" in lines:
                lines = lines.replace(':',' ')
                lines = lines.replace('(',' ')
                lines = lines.replace(')',' ')
                lines = lines.replace(',',' ')
                for sub_string in lines.split():
                    try:
                        numbers.append(float(sub_string))
                    except ValueError:
                        pass

        wcs_ra = (str(int(numbers[0]))+':'+str(int(numbers[1]))+':'+str(numbers[2]))
        wcs_dec = (str(int(numbers[3]))+':'+str(int(numbers[4]))+':'+str(numbers[5]))

        print("wcs_ra = ", wcs_ra )
        print("wcs_dec = ", wcs_dec )

        #####  get x,y slide coordinates of brightest gaia source ###########
        gaia_targ = get_XYslide_of_brightest_gaia(wcs_ra, wcs_dec, rotation_angle) # star_ID, xslide, yslide, mag
        print('gaia target = ', gaia_targ[1], gaia_targ[2], gaia_targ[3] )
    else:
        gaia_targ = [0, xslide, yslide, Mag] # use pre-calculated values

    
    main_prog_comms.stop_exposures() # exposures must be stopped before selecting slides
    sleep(5)
    main_prog_comms.set_slide(0) # slide0 for Mookodi
    #sleep(2)
    #main_prog_comms.initialise_slide()
    sleep(5) 
    if (float(gaia_targ[3]) > 10):
        main_prog_comms.set_exposure_time(4000)
    else:
        main_prog_comms.set_exposure_time(2000)
    sleep(5) # any less than 4 then the commands are too close and maybe missed
    # should replace this with a while state check 
    main_prog_comms.set_gotoXYcoords([str(int(gaia_targ[1])), str(int(gaia_targ[2]))])
    sleep(10) # slides can take longer than 10s but need to start exposures before guide_on command is sent
    main_prog_comms.start_exposures()
    sleep(5)
    #main_prog_comms.guide_on()
    #sleep(2)
    
#    main_prog_comms.disconnect() ### disconenct when finished
#    sleep(2)

    #main_prog_comms.initialise_slide()

    return()

def Guide_on():
    main_prog_comms.guide_on()
    sleep(2)
#    main_prog_comms.disconnect() ### disconenct when finished
#    sleep(2)
    return()

def Guide_off():
    main_prog_comms.guide_off()
    sleep(2)
#    main_prog_comms.disconnect() ### disconenct when finished
#    sleep(2)
    return()

def Exit_gracefully(signal, frame): # this should cause prgram to exit here on crt-C
    c_mookodi.close() # close socket to main_prog.c 
    sys.exit(0)


    
if __name__ == "__main__":
    signal.signal(signal.SIGINT, Exit_gracefully) # should capture crt_c 

    global G_connected



    ###################################################################
    # connect to main_prog
    ###################################################################

    main_prog_comms = Guider(host="10.2.2.31", test=False) # create an instance of the Guider class
    # 10.2.2.31 is 1ms1 where main_prog is excuted
    main_prog_comms.connect()
    sleep(2)
    main_prog_comms.set_exposure_time(2000) # first command fails so send this command to get comms working
    sleep(1)


    ###################################################################
    # create a socket object to listen to for connections from mookodi
    ###################################################################

    
    client_socket = socket.socket()
    client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # reserve a port to listen to
    port_main_prog = 12360
    # bind to the port
    client_socket.bind(('127.0.0.1', port_main_prog)) # note if IP field is empty it will listen for any computer on network
    # put the socket into listening mode
    client_socket.listen(1) # 1 means will only allow 1 connection to queue  
    # Establish connection with client.
    print("gaia_list: before accept")
    c_mookodi, addr = client_socket.accept()      # code hangs and waits here !!!!!!!!!!!!!!!!!! HANGS
    print('gaia_list: Got connection from mookodi', addr)
    G_connected  = True
    ###################################################################


    while True: # infinite loop - can only stop by systemctl stop
        while G_connected: 
            command = c_mookodi.recv(1024) # hangs here until data recieved or connection is broken
            print("gaia_list: command = ", command)
            if command == b'':
                G_connected = False
                c_mookodi.close() # close socket mookodi
                print("gaia_list: closed connection")
            if (G_connected and "Guide_on" in str(command)) :
                Guide_on()            
            if (G_connected and "Guide_off" in str(command)) :
                Guide_off()            
            if (G_connected and "pos_XYslides" in str(command)) :
                _, xslide, yslide, Mag = str(command).replace("'","").split()
                pos_XYslides(xslide, yslide, Mag)            

        print("gaia_list: before accept again")
        c_mookodi, addr = client_socket.accept()      # code hangs and waits here for new connection
        G_connected  = True
        sleep(1)
        #main_prog_comms.set_exposure_time(2000) # first command fails so send this command to get comms working
