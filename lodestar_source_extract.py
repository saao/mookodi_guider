#python3.10 -m pip install sep
#python3.10 -m pip install photutils

### note that this is run as a systemctl service on Mookodi
### because 1ms1 does not have latest python3 or all the astroutils etc


import signal # used to capture ctrl-c and exit nicely by closing connection
import sys # used by sys.exit
import time

import socket # used for socket comms with C code

import numpy as np
import math

from photutils import CircularAperture
import sep
import struct



# (291, 376)  with default 2x2 biining

def source_extraction():
    #image_data_array = np.genfromtxt(filename, dtype=float, delimiter=' ')
    bkg = sep.Background(image_data_array) # determines background count of image                                                         
    image_sub_bck = image_data_array - bkg # subtract bck
    low_values_flags = image_sub_bck < 0 # make -ve values 1
    image_sub_bck[low_values_flags] = 1 # make -ve values 1  make 1 because will be taking log below

    high_values_flags = image_sub_bck > 32000 # make big values =32000
    image_sub_bck[high_values_flags] = 32000 # make big values =32000

    binning = 1
    plate_scale = 0.2
    pixel_size_arc = plate_scale*binning
    aperture_size_arc = 2 #arcsec                                                                                             
    radius = aperture_size_arc/pixel_size_arc  #X arcsec width in px                                                          

    ##############################################################################################
    # finds sources in the raw bkd sub image
    ##############################################################################################
    temp = image_sub_bck[20:560, 20: 730] # avoide regions around edges
    temp = temp.copy(order='C') # need to set the congruant flag to TRUE ... goes to false in the sub array selection
    sources_orig = sep.extract(temp, thresh=1.5, minarea=(np.pi*radius**2)/3, err=bkg.globalrms)
    if (len(sources_orig['x']) == 0): # if no sources then return without proessing further
        results = ''
        results = results + '\n'
        results = results + '9999' + '\n'
        results = results + '9999' + '\n'
        results = results + '9999' + '\n'
        results = results + '9999' + '\n'
        results = results + '9999' + '\n' 
        c.sendall(results.encode()) # had to encode this to prevent "expected bytes error"
        return()                     
    sources_orig['x'] = sources_orig['x'] + 20 # add back 20 from ignoring image's border of 20
    sources_orig['y'] = sources_orig['y'] + 20
    no_or_orig_sources = len(sources_orig['x']) 

    #print("sources_orig['x,y']  ", sources_orig['x'], sources_orig['y'])
    
    ##############################################################################################
    # do basic aperture photometry on all stars
    ##############################################################################################
    flux, fluxerr, flag = sep.sum_circle(image_sub_bck, x = sources_orig['x'], y = sources_orig['y'],r = radius, err=bkg.globalrms, gain=1.0)
    maxflux_index = np.argmax(flux) ## find index of brightest star
    Xorig_pos_maxflux = sources_orig['x'][maxflux_index]
    Yorig_pos_maxflux = sources_orig['y'][maxflux_index]

    ##############################################################################################
    ##### calculate the FWHM of all of the extracted stars. ######
    ##############################################################################################
    FWHM, flag = sep.flux_radius(image_sub_bck, sources_orig['x'], sources_orig['y'], 6.*sources_orig['a'], 0.5, subpix=5)
    FWHM =  FWHM * 2
    
    #print(Xorig_pos_maxflux, Yorig_pos_maxflux, flux[maxflux_index], bkg.globalback, FWHM, FWHM[maxflux_index] )
    
    # write to socket main_prog:web_source_extract  x,y of brightest star and FMHM and maxflux and background 
    results = ''
    results = results + '\n'
    results = results + (format(float(Xorig_pos_maxflux),'8f') ) + '\n'
    results = results + (format(float(Yorig_pos_maxflux),'8f') ) + '\n'
    results = results + (format(float(flux[maxflux_index]),'8f') ) + '\n'
    results = results + (format(float(bkg.globalback),'8f') ) + '\n'
    results = results + (format(float(FWHM[maxflux_index]),'8f') ) + '\n' 
    c.sendall(results.encode()) # had to encode this to prevent "expected bytes error"
    #results = ''
    #results = results + '\n'
    #results = results + 'xc: ' + (format(float(Xorig_pos_maxflux),'8f') ) + '\n'
    #results = results + 'yc: ' + (format(float(Yorig_pos_maxflux),'8f') ) + '\n'
    #results = results + 'zc: ' + (format(float(flux[maxflux_index]),'8f') ) + '\n'
    #results = results + 'back: ' + (format(float(bkg.globalback),'8f') ) + '\n'
    #results = results + 'fwhm: ' + (format(float(FWHM[maxflux_index]),'8f') ) + '\n' 
    #c.sendall(results.encode()) # had to encode this to prevent "expected bytes error"
    
def Exit_gracefully(signal, frame): # this should cause prgram to exit here on crt-C
    c.close() # close socket to main_prog.c 
    sys.exit(0)

def receive_data(msglen):        
    global G_connected
    chunks = []
    bytes_recd = 0
    while bytes_recd < msglen:
        chunk = c.recv(min(msglen - bytes_recd, 2048)) # hangs here until data recieved or connection is broken
        if chunk == b'':
            G_connected = False
            c.close() # close socket to main_prog.c 
            return b''.join(chunks) #return()
        chunks.append(chunk)
        bytes_recd = bytes_recd + len(chunk)
    return b''.join(chunks)

if __name__ == '__main__':    
    signal.signal(signal.SIGINT, Exit_gracefully) # should capture crt_c 

    ###################################################################
    ####### wait for connection from client (main_prog) first  ###############
    # create a socket object to listen to for connections from   main_prog.c
    ###################################################################

    global G_connected

    client_socket = socket.socket()
    client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # reserve a port to listen to
    port_main_prog = 12348
    # bind to the port
    #client_socket.bind(('127.0.0.1', port_main_prog)) # note if IP field is empty it will listen for any computer on network
    client_socket.bind(('', port_main_prog)) # 10.2.2.31 is 1ms1suth.saao.ac.za
    # put the socket into listening mode
    client_socket.listen(1) # 1 means will only allow 1 connection to queue  
    # Establish connection with client.
    #print("before accept")
    c, addr = client_socket.accept()      # code hangs and waits here !!!!!!!!!!!!!!!!!! HANGS
    print('lodestar_source_exrt: Got connection from main_prog ', addr) 
    G_connected  = True
    ###################################################################


    
    img_h = 582 # image will always be 582, 752 even when binning
    img_w = 752
    bytes_per_pixel = 8
    image_size = img_w * img_h * bytes_per_pixel                



    while True:
        while G_connected: # infinite loop - exit with ctrl-C ... calls exit_gracefully
            pixels = receive_data(image_size)# hangs in this function on c.recv and will close socket if main_prog disconnects

            if (G_connected and type (pixels) == bytes) :
                #print(type (pixels) )
                pixels = [num[0] for num in struct.iter_unpack("L", pixels)]
                #####  put recieved pixels into a 2d array ##############
                image_data_array = np.zeros((img_h, img_w))
                pos=0
                for i in range(img_h):
                    for j in range(img_w):
                        image_data_array[i,j] = pixels[pos]
                        pos = pos + 1
            
                source_extraction() ## this does sextraction on 2d array and write results back to main_prog.c

        c.close() # close socket to main_prog.c ... should have already closed in recieve_data 
        print('lodestar_source_exrt: Closed connection from main_prog ', addr)
        c, addr = client_socket.accept()      # code hangs and waits here for new connection
        G_connected  = True
        print('lodestar_source_exrt: Got connection from main_prog ', addr)
