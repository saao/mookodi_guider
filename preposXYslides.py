#python3 -m pip install astroquery
# sep and photoutils were already istalled

# to run on mookodi use this virtual env:
#source /home/mookodi/development/WCS/pyenv/wcsenv/bin/activate

#from guider import Guider

######################
###### get XY slide coordinates ######
### input:  needs ACCURATE coordinates of target 
### returns: 6 * XY slides coordinates, North up_OR_down for each of center, narrowSlit, wideSlit
######################


import glob
import os
import argparse
import sys
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
import datetime
import numpy as np
import matplotlib.pyplot as plt
import astropy.visualization
zscaler = astropy.visualization.ZScaleInterval()
#from photutils import CircularAperture
#import sep
from collections import OrderedDict
import math 
from time import sleep
#import socket
#import signal # used to capture ctrl-c and exit nicely by closing connection


from astroquery.gaia import Gaia

def get_gaia_list(tel_ra_in_deg, tel_dec_in_deg):  #
    

    ##############################################
    # Get list of gaia source around tel ra and dec ... 40arcmins width 40arcmins height (actually 1x1 degree)
    ##############################################

    Gaia.MAIN_GAIA_TABLE = "gaiaedr3.gaia_source" # Select early Data Release 3
    coord = SkyCoord(ra=tel_ra_in_deg, dec=tel_dec_in_deg, unit=(u.degree, u.degree), frame='icrs')
    width = u.Quantity(1.0, u.deg) # 40arcmins = 0.667 degs
    height = u.Quantity(1.0, u.deg) # make it 1 degree to accommodate the 3 magic pixel coordinates with 1 query
    Gaia.ROW_LIMIT = 80000
    r = Gaia.query_object_async(coordinate=coord, width=width, height=height)

    gaia_star_info = np.rec.fromarrays([ r['source_id'].data,
                                r['ra'].data,
                                r['dec'].data,
                                r['phot_rp_mean_mag'].data],
                                dtype=None,names=['source_id','ra','dec','phot_rp_mean_mag'])
    print(" no_gaia_sources  : ", len(gaia_star_info) )        

    return(gaia_star_info)


def get_XYslide_of_brightest_gaia(tel_ra_in_deg, tel_dec_in_deg, rotator_angle, gaia_star_info):  #get_XYslide_of_brightest_gaia(wcs_ra, wcs_dec, rotator_angle)
    

    if ( (rotator_angle > -2.0) and (rotator_angle < 2.0) ): # rotator angle around 0
        #  only select stars with dec < tel_dec and brighter than 18 ... for rotang near zero, target is at the top
        half_gaia_star_info = (gaia_star_info[np.where((gaia_star_info['dec']<tel_dec_in_deg) & (gaia_star_info['phot_rp_mean_mag']<18)  )])
        print("     no_gaia_sources brighter than 18 and south of tel_dec  : ", len(half_gaia_star_info) )    
    elif ( (rotator_angle > 178) or (rotator_angle < -178) ): # rotator angle around 180
        #  only select stars with dec > tel_dec and brighter than 18 ... for rotang near 180, target is at the bottom
        half_gaia_star_info = (gaia_star_info[np.where((gaia_star_info['dec']>tel_dec_in_deg) & (gaia_star_info['phot_rp_mean_mag']<18)  )])
        print("     no_gaia_sources brighter than 18 and north of tel_dec  : ", len(half_gaia_star_info) )    
    

    #print(half_gaia_star_info)
    
    # convert gaia  RAs to dec corrected RAs. RA aleady in degrees so no need for factor of 15
    half_gaia_star_info['ra'] = half_gaia_star_info['ra']  * math.cos(tel_dec_in_deg*math.pi/180.)
    # convert tel  RA to dec corrected RA. RA aleady in degrees so no need for factor of 15
    tel_ra_in_deg_sky = tel_ra_in_deg * math.cos(tel_dec_in_deg*math.pi/180.)
    
    # subtract tel ra and dec from gaia coords 
    ra_dist =   half_gaia_star_info['ra'] - tel_ra_in_deg_sky
    dec_dist =  half_gaia_star_info['dec'] - tel_dec_in_deg
    
    #### see /Users/sbp/Lesedi/xyslides2ports/gaia_list/data.py   for how these conversions parameters were derived

    if ( (rotator_angle > -2.0) and (rotator_angle < 2.0) ): # rotator angle around 0
        xa = -2.8275789693768107e-05
        xc = 0.2692593231011655
        x_slide = (ra_dist-xc)/xa
        ya = 2.8279005498598875e-05
        yc = -0.4196063511127433
        y_slide = (dec_dist-yc)/ya
    elif ( (rotator_angle > 178) or (rotator_angle < -178) ): # rotator angle around 180
        xa = 2.8338318048924984e-05
        xc = -0.26546256702498733
        x_slide = (ra_dist-xc)/xa
        ya = -2.8441796558404097e-05
        yc = 0.4257036922377935
        y_slide = (dec_dist-yc)/ya

        
    ## overwrite the ra dec with x and y slide in the record array ... makes it easier to filter below
    half_gaia_star_info['ra'] = x_slide
    half_gaia_star_info['dec'] = y_slide


    #### filter to select stars only within xyslides   2000<x<14000 & 2000<y<6000   PLUS(note not AND)   2000<x<6000 & 2000<y<13000
    half_gaia_star_info_low=half_gaia_star_info[np.where( (half_gaia_star_info['ra']<14000)  & (half_gaia_star_info['ra']>2000))]
    #half_gaia_star_info_low=half_gaia_star_info_low[np.where( (half_gaia_star_info_low['dec']<8000) & (half_gaia_star_info_low['dec']>4000))]
    half_gaia_star_info_low=half_gaia_star_info_low[np.where( (half_gaia_star_info_low['dec']<12000) & (half_gaia_star_info_low['dec']>4000))]

    #half_gaia_star_info_left=half_gaia_star_info[np.where( (half_gaia_star_info['ra']<5000)  & (half_gaia_star_info['ra']>2000))]
    half_gaia_star_info_left=half_gaia_star_info[np.where( (half_gaia_star_info['ra']<8000)  & (half_gaia_star_info['ra']>2000))]
    half_gaia_star_info_left=half_gaia_star_info_left[np.where( (half_gaia_star_info_left['dec']<13000) & (half_gaia_star_info_left['dec']>4000))]


    try:
        max_bright_index_left = np.argmin(half_gaia_star_info_left['phot_rp_mean_mag'])
        brightest_left = half_gaia_star_info_left['phot_rp_mean_mag'][max_bright_index_left] # brightest star in left area
        max_bright_index_low = np.argmin(half_gaia_star_info_low['phot_rp_mean_mag'])  
        brightest_low = half_gaia_star_info_low['phot_rp_mean_mag'][max_bright_index_low] # brightest star in low area
    except ValueError:
        print("seems no gaia stars")
        return([1234, 4000,4000,16]) # return a 4000,4000 and hope a star is there.

    if (brightest_low < brightest_left ):
        return(half_gaia_star_info_low[max_bright_index_low])
    else:
        return(half_gaia_star_info_left[max_bright_index_left])


def pos_XYslides(target_ra, target_dec): 
    ###############################################
    #####  get x,y slide coordinates of brightest gaia source ###########
    ######   there are 6 possibilities ie 2 roations angles *  3 magic pixels  (center, narrow, wide)
    ###############################################


    XYslides_list = [0,0,0,0,0,0]

    telescope_coords = SkyCoord(target_ra, target_dec,unit=(u.hourangle, u.deg))
    tel_ra_in_deg = float(telescope_coords.to_string().split()[0])
    tel_dec_in_deg = float(telescope_coords.to_string().split()[1])


    ############################################
    # query the gaia archive ... only do this once and NOT for every rotation angle or slit position
    gaia_star_info = get_gaia_list(tel_ra_in_deg, tel_dec_in_deg)
    ############################################


    ######### magic pixel at center of ccd #################
    rotation_angle = 179.  # testing
    gaia_targ = get_XYslide_of_brightest_gaia(tel_ra_in_deg, tel_dec_in_deg, rotation_angle, gaia_star_info) # star_ID, xslide, yslide, mag
    print('center: gaia target = ', int(gaia_targ[1]), int(gaia_targ[2]), "{:.1f}".format(float(gaia_targ[3])), rotation_angle )
    XYslides_list[0]= 'center, ' + str(int(gaia_targ[1])) + ', ' + str(int(gaia_targ[2])) + ', ' +  str("{:.1f}".format(float(gaia_targ[3]))) + ', ' + str(rotation_angle) 
    print('  ')
    rotation_angle = 0.1  # testing
    gaia_targ = get_XYslide_of_brightest_gaia(tel_ra_in_deg, tel_dec_in_deg, rotation_angle, gaia_star_info) # star_ID, xslide, yslide, mag
    print('center: gaia target = ', int(gaia_targ[1]), int(gaia_targ[2]), "{:.1f}".format(float(gaia_targ[3])), rotation_angle )
    XYslides_list[1]= 'center, ' + str(int(gaia_targ[1])) + ', ' + str(int(gaia_targ[2])) + ', ' +  str("{:.1f}".format(float(gaia_targ[3]))) + ', ' + str(rotation_angle) 
    ###############################################
    print('  ')
    

    Wide_magicX = 613
    Wide_magicY = 240
    Narw_magicX = 613
    Narw_magicY = 585
    pixel_scale = 0.59
    CenterX = 512
    CenterY = 512    

    ######### magic pixel at narrow slit  #################


    rotation_angle = 179.  # testing,  north is down therefore slit is south of center, slit is more East of center
    Dec_at_cent = tel_dec_in_deg + ( (Narw_magicY-CenterY)*pixel_scale/3600. ) # ie dec at center has increased
    RA_at_cent = tel_ra_in_deg - ( ((Narw_magicX-CenterX)*pixel_scale/math.cos(tel_dec_in_deg*math.pi/180.))/3600. ) # ie RA at center has decreased
    gaia_targ = get_XYslide_of_brightest_gaia(RA_at_cent, Dec_at_cent, rotation_angle, gaia_star_info) # star_ID, xslide, yslide, mag
    print('narrow: gaia target = ', int(gaia_targ[1]), int(gaia_targ[2]), "{:.1f}".format(float(gaia_targ[3])), rotation_angle )
    XYslides_list[2]= 'narrow, ' + str(int(gaia_targ[1])) + ', ' + str(int(gaia_targ[2])) + ', ' +  str("{:.1f}".format(float(gaia_targ[3]))) + ', ' + str(rotation_angle) 
    print('  ')
    
    rotation_angle = 0.1  # testing
    Dec_at_cent = tel_dec_in_deg - ( (Narw_magicY-CenterY)*pixel_scale/3600. ) # ie dec at center has decreased
    RA_at_cent = tel_ra_in_deg + ( ((Narw_magicX-CenterX)*pixel_scale/math.cos(tel_dec_in_deg*math.pi/180.))/3600. ) # ie RA at center has increased
    gaia_targ = get_XYslide_of_brightest_gaia(RA_at_cent, Dec_at_cent, rotation_angle, gaia_star_info) # star_ID, xslide, yslide, mag
    print('narrow: gaia target = ', int(gaia_targ[1]), int(gaia_targ[2]), "{:.1f}".format(float(gaia_targ[3])), rotation_angle )
    XYslides_list[3]= 'narrow, ' + str(int(gaia_targ[1])) + ', ' + str(int(gaia_targ[2])) + ', ' +  str("{:.1f}".format(float(gaia_targ[3]))) + ', ' + str(rotation_angle) 
    print('  ')
    ###############################################    
    
    ######### magic pixel at wide slit  #################
    rotation_angle = 179.  # testing,  north is down therefore slit is south of center, slit is more East of center
    Dec_at_cent = tel_dec_in_deg + ( (Wide_magicY-CenterY)*pixel_scale/3600. ) # ie Dec at center has decreased
    RA_at_cent = tel_ra_in_deg - ( ((Wide_magicX-CenterX)*pixel_scale/math.cos(tel_dec_in_deg*math.pi/180.))/3600. ) # ie RA at center has decreased
    gaia_targ = get_XYslide_of_brightest_gaia(RA_at_cent, Dec_at_cent, rotation_angle, gaia_star_info) # star_ID, xslide, yslide, mag
    print('Wide: gaia target = ', int(gaia_targ[1]), int(gaia_targ[2]), "{:.1f}".format(float(gaia_targ[3])), rotation_angle )
    XYslides_list[4]= 'wide, ' + str(int(gaia_targ[1])) + ', ' + str(int(gaia_targ[2])) + ', ' +  str("{:.1f}".format(float(gaia_targ[3]))) + ', ' + str(rotation_angle) 
    print('  ')
    
    rotation_angle = 0.1  # testing
    Dec_at_cent = tel_dec_in_deg - ( (Wide_magicY-CenterY)*pixel_scale/3600. ) # ie dec at center has increased
    RA_at_cent = tel_ra_in_deg + ( ((Wide_magicX-CenterX)*pixel_scale/math.cos(tel_dec_in_deg*math.pi/180.))/3600. ) # ie RA at center has increased
    gaia_targ = get_XYslide_of_brightest_gaia(RA_at_cent, Dec_at_cent, rotation_angle, gaia_star_info) # star_ID, xslide, yslide, mag
    print('Wide: gaia target = ', int(gaia_targ[1]), int(gaia_targ[2]), "{:.1f}".format(float(gaia_targ[3])), rotation_angle )
    XYslides_list[5]= 'wide, ' + str(int(gaia_targ[1])) + ', ' + str(int(gaia_targ[2])) + ', ' +  str("{:.1f}".format(float(gaia_targ[3]))) + ', ' + str(rotation_angle) 
    print('  ')
    ###############################################    

    
    return(XYslides_list)

    
if __name__ == "__main__":
    ######################
    ###### get XY slide coordinates ######
    ### input:  needs ACCURATE coordinates of target 
    ### returns: 6 * XY slides coordinates, North up_OR_down for each of center, narrowSlit, wideSlit
    ######################


    ### open the pre_pos_XYslides_list.txt  ####
    ### if the target ra and dec is already in the list then do not bother to search for Gstar coordinates
    try:
        with open("pre_pos_XYslides_list.txt",'r') as f:
            pre_pos_targets = f.readlines()
        f.close()
    except:
        #f.close()
        print("pre_pos_XYslides_list.txt  does not exist")
        with open("pre_pos_XYslides_list.txt",'w') as f: # create the file with some dud data
            f.write("00:00:00 " + "00:00:00" + '\n')
            for index in range(6):  
                f.write("0, 0, 0, 0, 0" + '\n')
            f.close()
        with open("pre_pos_XYslides_list.txt",'r') as f: # and now reopen file with dud data
            pre_pos_targets = f.readlines()
        f.close()


    
    ################## create list of the setup_ files ####################
    ###### there will be a new list if setup_ files for observatioon requets for every night
    #file_list=glob.glob("/home/mookodi/development/xyslides_tests/scripts/setup_*")
    file_list=glob.glob("/home/observer/schedule/scripts/setup_*")
    sorted_file_list = sorted(file_list) # sort the list into alphabetical/numeric order
    ####################################################################################

    if len(sorted_file_list) == 0:
        print("no setup_ files in /home/observer/schedule/scripts/  \n")
        exit()
    

    file_number = 0
    while file_number < len(sorted_file_list):
        ### open the  setup_ files one at a time and read in the target coordinates ####
        with open(sorted_file_list[file_number],'r') as f:
            setup_lines = f.readlines()
        f.close()
        print(sorted_file_list[file_number])
        ######################################

        ##### extract the targ_ra and targ_dec in the target_acquisition section of the setup_ file  ############
        section = False
        for lines in setup_lines:
            if "target_acquisition" in lines:
                section = True
            if ("targ_ra" in lines) and (section) :
                target_ra = (lines[lines.index('": "')+len('": "'):lines.index('",')])
            if ("targ_dec" in lines) and (section) :
                target_dec = (lines[lines.index('": "')+len('": "'):lines.index('",')])

        print(target_ra, target_dec)

        ####### go through all the pos_targets in the list ######
        exists = False
        for lines in pre_pos_targets:
            if ":" in lines:
                pre_pos_target_ra = lines.split()[0]
                pre_pos_target_dec = lines.split()[1]

                #print(pre_pos_target_ra, pre_pos_target_dec)

                # for some reason these calculations are very slow
                telescope_coords1 = SkyCoord(target_ra, target_dec,unit=(u.hourangle, u.deg))
                telescope_coords2 = SkyCoord(pre_pos_target_ra, pre_pos_target_dec,unit=(u.hourangle, u.deg))
                separation = (telescope_coords1.separation(telescope_coords2).degree)   # test if within 10arcsecs of any object already in the list

                ######## use targ ra and dec to find xy slide coordinate of guide stars from gaia query (if not already within 10arcsecs of targets already in list) #####
                if separation < 0.00278:  # 10 arcsecs
                    exists = True

        if exists is False:  ####### the target is not in list so get Gstar coords from gaia and add to pre_pos_XYslides_list.txt ####
            XYslides_list = pos_XYslides(target_ra, target_dec) ##### get the Gstar Cen/Nar/Wid XYslide_coords Mag Rot_ang
            with open("pre_pos_XYslides_list.txt",'a') as f:
                f.write(target_ra + " " + target_dec + '\n')
                for index in range(len(XYslides_list)):  #### append it to pre_pos_XYslides_list.txt
                    f.write(XYslides_list[index] + '\n')
            f.close()

        file_number = file_number + 1 # go to next setup_  file


        
